package saacanvas;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import saakontrolleri.SaaKontrolleri_IF;

/**
 * SaaCanvas-luokka kuvan piirt�mist� varten.
 *
 * @author Antti Pasanen
 *
 */
public class SaaCanvas extends Canvas implements SaaCanvas_IF {

	/*
	 * Luettavat tietueet.
	 */
	private final static String T_MIN = "obs-obs-1-1-tmin";
	private final static String T_MAX = "obs-obs-1-1-tmax";
	private final static String T_AVG = "obs-obs-1-1-tday";

	/*
	 * Vakioarvot kuvalle.
	 */
	private final static int PAIVAT_MAX = 30; // montako p�iv�� max kuvaan mahtuu
	private final static double RAJA_BUFFER = 25; // pikseleit� reunan ja akseleiden v�lill�
	private final static double KRD_AKSELI_LEVEYS = 1; //koord. akselin leveys
	private final static double DFT_ASTEET = 20; // montako astetta akselilla on
	private final static int DFT_X = 800; // koko x-pikseli�
	private final static int DFT_Y = 600; // koko y-pikseli�


	private double akseli_asteet; // montako astetta akselilla on (0 - +/- asteet)
	private double korkeus; // koord. akselin korkeus 0-asteen kohdalta.
	private double pylvas_leveys; // pylv�iden leveys, pikseleiss�.

	private ArrayList<Double> t_min;
	private ArrayList<Double> t_max;
	private ArrayList<Double> t_avg;

	private String paikanNimi;
	private LocalDate aloitusPVM;
	private LocalDate lopetusPVM;

	private SaaKontrolleri_IF saaKontrolleri;
	private GraphicsContext gc;


	/**
	 * Konstruktori vakioasetuksilla.
	 */
	public SaaCanvas() {
		super(DFT_X, DFT_Y);
		gc = this.getGraphicsContext2D();
		korkeus = this.getHeight() / 2 - KRD_AKSELI_LEVEYS / 2 - RAJA_BUFFER;
		pylvas_leveys = (DFT_X - 2 * RAJA_BUFFER) / PAIVAT_MAX;
		saaKontrolleri = null;
		akseli_asteet = DFT_ASTEET;
	}

	/**
	 * Konstruktori kuvan koon ja astem��rien mukaan.
	 *
	 * @param x X-koko pikseleiss�.
	 * @param y Y-koko pikseleiss�.
	 * @param asteet Montako l�mp�astetta akselilla on (yht. 2 x m��r�).
	 *
	 */
	public SaaCanvas(int x, int y, int asteet) {
		super(x, y);
		gc = this.getGraphicsContext2D();
		korkeus = this.getHeight() / 2 - KRD_AKSELI_LEVEYS / 2 - RAJA_BUFFER;
		pylvas_leveys = (x - 2 * RAJA_BUFFER) / PAIVAT_MAX;
		saaKontrolleri = null;
		akseli_asteet = asteet;
	}

	/**
	 * Asettaa kontrollerin. Suoritettava ennen piirt�mist�.
	 *
	 * @param sk SaaKontrolleri_IF
	 */
	public void setSaaKontrolleri(SaaKontrolleri_IF sk) {
		saaKontrolleri = sk;
	}


	/**
	 * Piirt�� kuvan.
	 */
	public void piirra() {
		piirraPosArvot();
		piirraNegArvot();
		piirraKeskilampo();
		piirraKoordinaatit();
		kirjoitaPaikanNimi();
		hiirenLiike();
	}


	/**
	 * Piirt�� koordinaattiakselit.
	 */
	private void piirraKoordinaatit() {
		gc.setFill(Color.BLACK);

		gc.fillRect(RAJA_BUFFER - KRD_AKSELI_LEVEYS,
					RAJA_BUFFER,
					KRD_AKSELI_LEVEYS,
					this.getHeight() - 2 * RAJA_BUFFER);

		gc.fillRect(RAJA_BUFFER - KRD_AKSELI_LEVEYS,
				this.getHeight() / 2 - KRD_AKSELI_LEVEYS / 2,
				this.getWidth() - 2 * RAJA_BUFFER,
				KRD_AKSELI_LEVEYS);
	}


	/**
	 * Piirt�� yli 0-asteen sijoittuvat osat pylv��st�.
	 */
	private void piirraPosArvot() {
		gc.setFill(Color.ORANGERED);

		for (int i = 0; i < Math.min(PAIVAT_MAX,
				ChronoUnit.DAYS.between(aloitusPVM, lopetusPVM)); i++) {
			if (t_max.get(i) > 0) {
				gc.fillRect(i * pylvas_leveys + RAJA_BUFFER,
							RAJA_BUFFER + korkeus - t_max.get(i) * korkeus / akseli_asteet,
							pylvas_leveys,
							(t_max.get(i) - Math.max(0, t_min.get(i))) * korkeus / akseli_asteet);
			}
		}
	}


	/**
	 * Piirt�� alle 0-asteen sijoittuvat osat pylv��st�.
	 */
	private void piirraNegArvot() {
		gc.setFill(Color.CYAN);

		for (int i = 0; i < Math.min(PAIVAT_MAX,
				ChronoUnit.DAYS.between(aloitusPVM, lopetusPVM)); i++) {
			if (t_min.get(i) < 0) {
				gc.fillRect(i * pylvas_leveys + RAJA_BUFFER,
							RAJA_BUFFER + korkeus - Math.min(0, t_max.get(i)),
							pylvas_leveys,
							Math.abs(t_min.get(i)) * korkeus / akseli_asteet);
			}
		}
	}


	/**
	 * Seuraa hiirenliikett� alustalla. Kun hiiri on pylv�iden kohdalla,
	 * l�hett�� viestin joka sis�lt�� ko. kohdalla olevan pylv��n tiedot.
	 */
	private void hiirenLiike() {
		this.setOnMouseMoved(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent me) {
				int xloc = (int) ((me.getSceneX() - RAJA_BUFFER) / pylvas_leveys);
				if (me.getSceneX() > RAJA_BUFFER &&
					me.getSceneX() < pylvas_leveys * PAIVAT_MAX + RAJA_BUFFER) {
					if (me.getSceneY() > RAJA_BUFFER) {
						saaKontrolleri.ilmoita(String.format(
								"%s: Min: %+3.1f Max: %+3.1f Avg: %+3.1f",
								aloitusPVM.plusDays(xloc), t_min.get(xloc),
								t_max.get(xloc), t_avg.get(xloc)));
					}
				}
			}
		});
	}


	/**
	 * Piirt�� viivan keskil�mp�tilojen kautta.
	 */
	private void piirraKeskilampo() {
		gc.setFill(Color.BLACK);

		double alkuX, alkuY, loppuX, loppuY;

		for (int i = 1; i < Math.min(PAIVAT_MAX,
				ChronoUnit.DAYS.between(aloitusPVM, lopetusPVM)); i++) {
			alkuX = RAJA_BUFFER + (i - 0.5) * pylvas_leveys;
			loppuX = alkuX + pylvas_leveys;

			alkuY = this.getHeight() / 2 - t_avg.get(i - 1) * korkeus / akseli_asteet;
			loppuY = this.getHeight() / 2- t_avg.get(i) * korkeus / akseli_asteet;

			gc.strokeLine(alkuX, alkuY, loppuX, loppuY);
		}
	}


	/**
	 * Kirjoittaa paikannimen ja p�iv�m��r�t kuvaan.
	 */
	private void kirjoitaPaikanNimi() {
		gc.setFill(Color.BLACK);
		gc.setTextAlign(TextAlignment.CENTER);
		gc.setTextBaseline(VPos.CENTER);
		gc.fillText(paikanNimi + "\n" + aloitusPVM + " - " +
					aloitusPVM.plusDays(PAIVAT_MAX - 1),
					this.getWidth()/2, RAJA_BUFFER / 2);
	}

	/**
	 * Tyhjent�� kuvan.
	 */
	public void tyhjenna() {
		gc.clearRect(0, 0, this.getWidth(), this.getHeight());
	}

	/**
	 * Lukee tarvittavat tiedot kuvan piirt�mist� varten muistiin.
	 */
	public void lueData() {
		t_min = saaKontrolleri.getTaulukko(T_MIN);
		t_max = saaKontrolleri.getTaulukko(T_MAX);
		t_avg = saaKontrolleri.getTaulukko(T_AVG);

		paikanNimi = saaKontrolleri.getPaikanNimi();
		aloitusPVM = saaKontrolleri.getAloitusPVM();
		lopetusPVM = saaKontrolleri.getLopetusPVM();
	}
}
