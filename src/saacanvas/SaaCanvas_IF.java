package saacanvas;

import saakontrolleri.SaaKontrolleri_IF;

/**
 * Piirtoalusta saaohjelman kuvan luonnille.
 *
 * @author Antti Pasanen
 *
 */
public interface SaaCanvas_IF {

	/**
	 * Asettaa kontrollerin.
	 *
	 * @param sk SaaKontrolleri_IF
	 */
	public abstract void setSaaKontrolleri(SaaKontrolleri_IF sk) ;
	/**
	 * Piirt�� kuvan.
	 */
	public abstract void piirra() ;
	/**
	 * Tyhjent�� kuvan.
	 */
	public abstract void tyhjenna();
	/**
	 * Lukee tarvittavat tiedot kuvan piirt�mist� varten muistiin.
	 */
	public abstract void lueData() ;
}
