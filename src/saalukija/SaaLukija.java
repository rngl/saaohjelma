package saalukija;
import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Lukija-luokka. Lukee annetusta XML-tiedostosta datan muistiin.
 *
 * @author Antti Pasanen
 *
 */
public class SaaLukija implements SaaLukija_IF{

	/*
	 * Luettavat elementit ja attribuutti.
	 */
    private final static String ATTRIBUUTTI = "gml:id";
    private final static String ALOITUS_PVM = "gml:beginPosition";
    private final static String LOPETUS_PVM = "gml:endPosition";
    private final static String PAIKAN_NIMI = "gml:name";
    private final static String SARJAN_NIMI = "wml2:MeasurementTimeseries";
    private final static String ARVO = "wml2:value";
    //private final static String PVM = "wml2:time"; // ei käytetty

    private HashMap<String, ArrayList<Double>> datamap;

    private String sarja;
    private LocalDate aloitusPVM;
    private LocalDate lopetusPVM;
    private String paikanNimi;

    /**
     * Konstruktori.
     */
    public SaaLukija() {
        datamap = new HashMap<String, ArrayList<Double>>();
    }

	/**
	 * Lukee annetun tiedoston muistiin Java DOM parserin rakentamaan
	 * puu-muotoon ja käynnistää tiedoston läpikäymisalgoritmin.
	 *
	 * @param tiedosto Luettava tiedosto.
	 */
    public void lue(File tiedosto) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(tiedosto);
            doc.getDocumentElement().normalize();
            Node root = doc.getDocumentElement();

            lueNode(root);

        }
        catch(Exception e) {

        }
    }

    /**
     * XML-tiedoston läpikäyntialgoritmi. Saa parametrina tutkittavan elementin,
     * tekee listan elementin sisäelementeistä ja kutsuu itseään rekursiivisesti
     * niihin. Lopuksi tarkastaa elementin tiedot lueNode-metodilla.
     *
     * @param node XML-tiedoston elementti.
     */
    private void lueNode(Node node) {
        NodeList nodeLista = node.getChildNodes();

        for (int i = 0; i < nodeLista.getLength(); i++) {
            if (nodeLista.item(i).getNodeType() == Node.ELEMENT_NODE) {
                lueElement((Element) nodeLista.item(i));
                lueNode(nodeLista.item(i));
            }
        }
    }

    /**
     * Käy läpi XML-elementin tiedot. Jos elementin nimi, tai attribuutti,
     * vastaa jotain tärkeää, luetaan tiedot muistiin.
     *
     * @param e Tarkasteltava elementti XML-tiedostosta.
     */
    private void lueElement(Element e) {
        switch(e.getNodeName()) {
            case PAIKAN_NIMI:
                paikanNimi = e.getTextContent();
                break;
            case ALOITUS_PVM:
                aloitusPVM = pvmKonversio(e.getTextContent());
                break;
            case LOPETUS_PVM:
                lopetusPVM = pvmKonversio(e.getTextContent());
                break;
            case SARJAN_NIMI:
                datamap.put(e.getAttribute(ATTRIBUUTTI), new ArrayList<Double>());
                sarja = e.getAttribute(ATTRIBUUTTI);
                break;
            case ARVO:
                datamap.get(sarja).add(Double.valueOf(e.getTextContent()));
                break;
            default:
                break;
        }
    }

    /**
     * Muokkaa tiedostosta luetusta päivämäärästa LocalDate olion.
     *
     * @param s Luettu päivämäärä.
     * @return LocalDate olio päivämäärästä.
     */
    private LocalDate pvmKonversio(String s) {
        int vuosi = Integer.valueOf(s.substring(0,4));
        int kuukausi = Integer.valueOf(s.substring(5,7));
        int paiva = Integer.valueOf(s.substring(8,10));

        return LocalDate.of(vuosi, kuukausi, paiva);
    }

    /**
     * Palauttaa arvotaulukon annetulla tunnuksella.
     *
     * @param key Tunnus.
     * @return Arvotaulukko.
     */
    public ArrayList<Double> getTaulukko(String key) {
        return datamap.get(key);
    }

    /**
     * Palauttaa tiedostossa määritellyn paikannimen.
     *
     * @return Paikannimi.
     */
    public String getPaikanNimi() {
        return paikanNimi;
    }

    /**
     * Palauttaa tiedostossa määritellyn aloituspäivämäärän.
     *
     * @return Aloituspäivämäärä.
     */
    public LocalDate getAloitusPVM() {
        return aloitusPVM;
    }

    /**
     * Palauttaa tiedostossa määritellyn lopetuspäivämäärän.
     *
     * @return Lopetuspäivämäärä.
     */
    public LocalDate getLopetusPVM() {
        return lopetusPVM;
    }
}
