package saalukija;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Rajapinta SaaLukija_IF XML-tiedoston lukemista varten.
 *  *
 * @author Antti Pasanen
 *
 */
public interface SaaLukija_IF {

	/**
	 * Lukee annetun XML-tiedoston muistiin.
	 *
	 * @param tiedosto Luettava tiedosto.
	 */
	public abstract void lue(File tiedosto);
	/**
	 * Palauttaa arvotaulukon annetulla tunnuksella.
	 *
	 * @param key Tunnus.
	 * @return Arvotaulukko.
	 */
	public abstract ArrayList<Double> getTaulukko(String key);
	/**
	 * Palauttaa tiedostossa määritellyn paikannimen.
	 *
	 * @return Paikannimi.
	 */
	public abstract String getPaikanNimi();
	/**
	 * Palauttaa tiedostossa määritellyn aloituspäivämäärän.
	 *
	 * @return Aloituspäivämäärä.
	 */
	public abstract LocalDate getAloitusPVM();
	/**
	 * Palauttaa tiedostossa määritellyn lopetuspäivämäärän.
	 *
	 * @return Lopetuspäivämäärä.
	 */
	public abstract LocalDate getLopetusPVM();
}
