package saakontrolleri;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Rajapinta Saaohjelman kontrollerille. Toteuttaa luokkienvälisen
 * kommunikoinnin.
 *
 * @author Antti Pasanen
 *
 */
public interface SaaKontrolleri_IF {

	/**
	 * Käskee lukijaa lukemaan tiedot anneusta tiedostosta.
	 *
	 * @param f Luettava tiedosto.
	 */
	public abstract void avaa(File f);
	/**
	 * Käskee canvasta tyhjentämään kuvan.
	 *
	 */
	public abstract void tyhjennaKuva();
	/**
	 * Käskee canvasta lukemaan tiedot muistiin.
	 */
	public abstract void luoDataKuva();
	/**
	 * Käskee canvasta piirtämään kuvan.
	 *
	 */
	public abstract void piirraKuva();
	/**
	 * Lähettää tekstiviestin käyttöliittymälle.
	 *
	 * @param ilmoitus Viesti.
	 */
	public abstract void ilmoita(String ilmoitus);
	/**
	 * Palauttaa lukijalta arvotaulukon annetulla tunnisteella.
	 *
	 * @param key Tunniste.
	 * @return Arvotaulukko.
	 */
	public abstract ArrayList<Double> getTaulukko(String key);
	/**
	 * Palauttaa lukijalta paikannimen.
	 *
	 * @return Paikannimi.
	 */
	public abstract String getPaikanNimi();
	/**
	 * Palauttaa lukijalta aloituspäivämäärän.
	 *
	 * @return Aloituspäivämäärä.
	 */
	public abstract LocalDate getAloitusPVM();
	/**
	 * Palauttaa lukijalta lopetuspäivämäärän.
	 *
	 * @return Lopetuspäivämäärä.
	 */
	public abstract LocalDate getLopetusPVM();
}
