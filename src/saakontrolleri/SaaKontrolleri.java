package saakontrolleri;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;

import saacanvas.SaaCanvas_IF;
import saagui.SaaGUI_IF;
import saalukija.SaaLukija_IF;

/**
 *
 * Kontrolleri-luokka SaaCanvas-, SaaGUI- ja SaaLukija-luokkien väliseen
 * kommunikointiin.
 *
 * @author Antti Pasanen
 *
 */
public class SaaKontrolleri implements SaaKontrolleri_IF {

	private SaaLukija_IF sl;
	private SaaGUI_IF sgui;
	private SaaCanvas_IF sc;

	/**
	 * Konstruktori.
	 *
	 * @param sl SaaLukija_IF
	 * @param sgui SaaGUI_IF
	 * @param sc SaaCanvas_IF
	 */
	public SaaKontrolleri(SaaLukija_IF sl, SaaGUI_IF sgui, SaaCanvas_IF sc) {
		this.sgui = sgui;
		this.sl = sl;
		this.sc = sc;
		sc.setSaaKontrolleri(this);
	}

	/**
	 * Käskee lukijaa lukemaan tiedot anneusta tiedostosta.
	 *
	 * @param f Luettava tiedosto.
	 */
	public void avaa(File f) {
		sl.lue(f);
	}

	/**
	 * Käskee canvasta tyhjentämään kuvan.
	 *
	 */
	public void tyhjennaKuva() {
		sc.tyhjenna();
	}

	/**
	 * Käskee canvasta lukemaan tiedot muistiin.
	 */
	public void luoDataKuva() {
		sc.lueData();
	}

	/**
	 * Käskee canvasta piirtämään kuvan.
	 *
	 */
	public void piirraKuva() {
		sc.piirra();
	}

	/**
	 * Lähettää tekstiviestin käyttöliittymälle.
	 *
	 * @param ilmoitus Viesti.
	 */
	public void ilmoita(String ilmoitus) {
		sgui.ilmoita(ilmoitus);
	}

	/**
	 * Palauttaa lukijalta arvotaulukon annetulla tunnisteella.
	 *
	 * @param key Tunniste.
	 * @return Arvotaulukko.
	 */
	public ArrayList<Double> getTaulukko(String key) {
		return sl.getTaulukko(key);
	}

	/**
	 * Palauttaa lukijalta paikannimen.
	 *
	 * @return Paikannimi.
	 */
	public String getPaikanNimi() {
		return sl.getPaikanNimi();
	}

	/**
	 * Palauttaa lukijalta aloituspäivämäärän.
	 *
	 * @return Aloituspäivämäärä.
	 */
	public LocalDate getAloitusPVM() {
		return sl.getAloitusPVM();
	}

	/**
	 * Palauttaa lukijalta lopetuspäivämäärän.
	 *
	 * @return Lopetuspäivämäärä.
	 */
	public LocalDate getLopetusPVM() {
		return sl.getLopetusPVM();
	}

}
