package saagui;


import java.io.File;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import saacanvas.SaaCanvas;
import saacanvas.SaaCanvas_IF;
import saakontrolleri.SaaKontrolleri;
import saakontrolleri.SaaKontrolleri_IF;
import saalukija.SaaLukija;
import saalukija.SaaLukija_IF;


/**
 *
 * JavaFX-käyttöliittymä saaohjelmalle.
 *
 * @author Antti Pasanen
 *
 */
public class SaaGUI extends Application implements SaaGUI_IF {

	private SaaKontrolleri_IF saaKontrolleri;
	private SaaCanvas_IF saaCanvas;
	Text teksti;

	public void init() {
		SaaLukija_IF sl = new SaaLukija();
		saaCanvas = new SaaCanvas(800, 600, 20);
		saaKontrolleri = new SaaKontrolleri(sl, this, saaCanvas);
	}

	public void start(Stage primaryStage) throws Exception {
		try {
			// Ilmoituskenttä
			teksti = new Text("");
			teksti.setStyle("-fx-font: 22 arial; -fx-base: black");
			teksti.setTextOrigin(VPos.CENTER);
			teksti.setTextAlignment(TextAlignment.LEFT);

			// Menu
			MenuBar menubar = new MenuBar();
			Menu menu = new Menu("Tiedosto");
			MenuItem avaa = new MenuItem("Avaa");
			menu.getItems().addAll(avaa);
			menubar.getMenus().addAll(menu);

			// Tiedostonvalitsija
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Valitse tiedosto");

			// Tiedostonavaus
			avaa.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(final ActionEvent e) {
					File file = fileChooser.showOpenDialog(primaryStage);
					if (file != null) {
						try {
							saaKontrolleri.tyhjennaKuva();
							saaKontrolleri.avaa(file);
							saaKontrolleri.luoDataKuva();
							saaKontrolleri.piirraKuva();
							ilmoita("");
						}
						catch(Exception e2) {
							ilmoita("Tiedoston avaus epäonnistui.");
						}
					}
				}
			});

			//Paneelien asettelu
			BorderPane bordis = new BorderPane();
			bordis.setTop(menubar);
			bordis.setCenter((Node) saaCanvas);
			bordis.setBottom(teksti);

			Scene scene = new Scene(bordis);
			primaryStage.setTitle("SääOhjelma");
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Vastaanottaa tekstiviesti ja tulostaa se ruudun pohjalla.
	 *
	 * @param ilmoitus Viesti.
	 */
	public void ilmoita(String ilmoitus) {
		teksti.setText(ilmoitus);
	}


	public static void main(String[] args) {
		launch(args);
	}
}
