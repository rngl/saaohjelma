package saagui;


/**
 * Rajapinta saaohjelman käyttöliittymälle.
 *
 * @author Antti Pasanen
 *
 */
public interface SaaGUI_IF {

	/**
	 * Vastaanottaa tekstiviesti.
	 *
	 * @param ilmoitus Viesti.
	 */
	public abstract void ilmoita(String ilmoitus);
}